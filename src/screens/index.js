import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import AboutScreen from './AboutScreen';
import AddScreen from './AddScreen';
import LoginScreen from './LoginScreen';
import ProjectScreen from './ProjectScreen';
import Register from './Register';
import SkillScreen from './skillScreen';


const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();


const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name='Skill Screen' component={SkillScreen} />
    <Tabs.Screen name='Project Screen' component={ProjectScreen} />
    <Tabs.Screen name='Add Screen' component={AddScreen} />
  </Tabs.Navigator>
)

const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name='Tabs Screen' component={TabsScreen}/>
    <Drawer.Screen name='About Screen' component={AboutScreen}/>
  </Drawer.Navigator>
)

export default class Index extends React.Component{
  render(){
    return(
    <NavigationContainer >
      <RootStack.Navigator initialRouteName="LoginScreen" headerMode='none'>
        <RootStack.Screen 
          name='LoginScreen'
          component={LoginScreen}
          />
        <RootStack.Screen 
          name='Register'
          component={Register}
          />
        <RootStack.Screen
          name='Drawer'
          component={DrawerScreen}
          />
      </RootStack.Navigator>
    </NavigationContainer>
    )
  }
}