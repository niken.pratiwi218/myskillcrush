import React from 'react';
import { View, Text,Image, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillCard extends React.Component{
  render(){
  let skills = this.props.skills;

  return(
    <TouchableOpacity style={styles.CategoryOfSkill}>
        <Icon style={styles.Icon} name={skills.iconName} size={80} color='#003366'/>
        
        <View style={styles.containerBox}>
          <Text style={styles.SkillName}>{skills.skillName}</Text>
          <Text style={styles.Category}>{skills.categoryName}</Text>
          <Text style={styles.persen}>{skills.percentageProgress}</Text>
        </View>

        <Icon style={styles.panah} name='chevron-right' size={100} color='#003366' />
    </TouchableOpacity>
  )
  }
}

const styles= StyleSheet.create({
  CategoryOfSkill: {
    width: '100%',
    height: 129,
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5
  },
  containerBox:{
    height:129,
    width:230,
    paddingHorizontal:10,
    justifyContent: 'center'
  },
  Icon:{
    marginVertical: 25,
    height:80,
    width:80,
  },
  SkillName:{
    fontSize:24,
    fontWeight: 'bold',
    color:'#003366'
  },
  Category:{
    fontSize:16,
    fontWeight:'bold',
    color:'#3EC6FF'
  },
  persen:{
    fontSize:48,
    fontWeight:'bold',
    color:'#FFFFFF',
    alignSelf:'flex-end'
  },
  panah: {
    width: 80,
  }

})