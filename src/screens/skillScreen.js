import React from 'react';
import {View,Image, Text, TouchableOpacity, StyleSheet, ScrollView, TextInput, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import data from '../api/skillData.json';
import SkillCard from './skillCard';

export default class skillScreen extends React.Component{
render(){
  return(
      <View style={styles.container}>
        <View style ={styles.navBar}>
          <View style={styles.rightNav}>
            <Image source={require('../assets/image/Logo.png')} style={{width:50, height:51, alignSelf: 'center'}}/>
            </View>
          <View style={styles.Hello}>
            <Icon style={styles.Icon} name='account-circle' size={32} color='#3EC6FF'/>
            <View style={styles.Name}>
            <Text style={{fontSize: 12, color: 'black'}}>Hai,</Text>
            <Text style={{fontSize: 16, color:'#003366'}}>Niken Pratiwi</Text>
            </View>
          </View>
          <Text style={{fontSize: 36, color:'#003366'}}>SKILL</Text>
          <View style={{
                borderBottomColor: '#3EC6FF',
                borderBottomWidth: 4,
              }}/>
        </View>
        <ScrollView style={styles.body}>
          
          <View style={styles.ListSkill}>
           <TouchableOpacity>
              <View style={styles.ListSkill1}>
                <Text style={styles.ListSkillText}>Library / Framework</Text>
              </View>
           </TouchableOpacity>
           <TouchableOpacity>
              <View style={styles.ListSkill2}>
                <Text style={styles.ListSkillText}>Bahasa Pemrograman</Text>
              </View>
           </TouchableOpacity>
           <TouchableOpacity>
              <View style={styles.ListSkill3}>
                <Text style={styles.ListSkillText}>Teknologi</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.Skill}>
              <FlatList
                data={data.items}
                renderItem={(skills)=><SkillCard skills={skills.item}/>}
                keyExtractor={(item)=>item.toString()}
              />
          </View>

        </ScrollView>
      </View>
    
  )
};
};

const styles= StyleSheet.create ({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  navBar: {
    height: 155,
    width: '100%',
    paddingTop: 5,
  },
  Hello: {
    width: 147,
    height: 33,
    flexDirection: 'row',
    marginBottom: 15
  },
  Name:{
    justifyContent: 'center',
    paddingLeft: 10
  },
  body: {
    flex:1
  },
  ListSkill:{
    height: 32,
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  ListSkillText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#003366'
  },
  ListSkill1: {
    height: 32,
    width: 125,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: '#B4E9FF'
  },
  ListSkill2: {
    height: 32,
    width: 136,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: '#B4E9FF'
  },
  ListSkill3: {
    height: 32,
    width: 70,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: '#B4E9FF'
  },
  Skill:{
    flex:1,
    width:'100%',
    flexDirection: 'column'
  },
  
})
