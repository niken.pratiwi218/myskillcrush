import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, TextInput} from 'react-native';
import AboutScreen from './AboutScreen';
import Logo from './logo';

export default class LoginScreen extends React.Component{
  render() {
    return (
      <View style={styles.container}>
      <Logo/>
        <Text style={styles.LoginTittle}>Login</Text>
        <View style={styles.Box}>
          <Text style= {styles.Title}>Username / Email</Text>
          <TextInput style={styles.inputBox} />
        </View>
        <View style={styles.Box}>
          <Text style= {styles.Title}>Password</Text>
          <TextInput style={styles.inputBox} />
        </View>
        <TouchableOpacity style={styles.button} onPress={() => {this.props.navigation.navigate('Drawer')}}>
          <Text style= {styles.Text}>Masuk</Text>
        </TouchableOpacity>
        <Text style={{fontSize: 24, color: '#3EC6FF', margin: 20}} >atau</Text>
        <TouchableOpacity style={styles.button2} onPress={() => {this.props.navigation.navigate('Register')}}>
          <Text style= {styles.Text}>Daftar ?</Text>
        </TouchableOpacity>
      </View>

    )
  };
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  LoginTittle: {
    fontSize: 24,
    color: '#003366',
    paddingTop: 40,
    paddingBottom: 20
  },
  Box: {
    width: 294,
    height:71,
    marginTop: 15,
  },
  Title: {
    fontSize: 16,
    color: '#003366'
  },
  inputBox:{
    width: 294,
    height: 48,
    borderWidth: 1,
    borderColor: '#003366'
  },
  button: {
    width: 140,
    height:40,
    marginTop: 35,
    alignItems: 'center',
    backgroundColor:'#3EC6FF',
    borderRadius: 16
  },
  Text:{
    fontSize: 24,
    color: '#FFFFFF',
  },
  button2: {
    width: 140,
    height:40,
    alignItems: 'center',
    backgroundColor:'#003366',
    borderRadius: 16
  },

})