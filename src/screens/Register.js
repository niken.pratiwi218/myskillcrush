import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, ScrollView, TextInput} from 'react-native';

import Logo from './logo';


export default class Register extends React.Component{
  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
      <Logo/>
        <Text style={styles.RegisterTittle}>Register</Text>
        <View style={styles.Box}>
          <Text style= {styles.Title}>Username</Text>
          <TextInput style={styles.inputBox} />
        </View>
        <View style={styles.Box}>
          <Text style= {styles.Title}>Email</Text>
          <TextInput style={styles.inputBox} />
        </View>
        <View style={styles.Box}>
          <Text style= {styles.Title}>Password</Text>
          <TextInput style={styles.inputBox} />
        </View>
        <View style={styles.Box}>
          <Text style= {styles.Title}>Ulangi Password</Text>
          <TextInput style={styles.inputBox} />
        </View>
        <TouchableOpacity style={styles.button} onPress={() => {this.props.navigation.navigate('Drawer')}}>
          <Text style= {styles.Text}>Daftar</Text>
        </TouchableOpacity>
        <Text style={{fontSize: 24, color: '#3EC6FF', margin: 20}} >atau</Text>
        <TouchableOpacity style={styles.button2} onPress={() => {this.props.navigation.navigate('LoginScreen')}}>
          <Text style= {styles.Text}>Masuk ?</Text>
        </TouchableOpacity>
      </View>
      </ScrollView>
    )
  };
};


const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    paddingBottom: 50
  },
  RegisterTittle: {
    fontSize: 24,
    color: '#003366',
    paddingTop: 40,
    paddingBottom: 20
  },
  Box: {
    width: 294,
    height:71,
    marginTop: 15,
  },
  Title: {
    fontSize: 16,
    color: '#003366'
  },
  inputBox:{
    width: 294,
    height: 48,
    borderWidth: 1,
    borderColor: '#003366'
  },
  button: {
    width: 140,
    height:40,
    marginTop: 35,
    alignItems: 'center',
    backgroundColor:'#003366',
    borderRadius: 16
  },
  Text:{
    fontSize: 24,
    color: '#FFFFFF',
  },
  button2: {
    width: 140,
    height:40,
    alignItems: 'center',
    backgroundColor:'#3EC6FF',
    borderRadius: 16
  },

})