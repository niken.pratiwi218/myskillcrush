import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {View, Text, TouchableOpacity, StyleSheet, ScrollView} from 'react-native';

export default class About extends React.Component{
  render(){
    return (
      <ScrollView>
      <View style={styles.container}>
         <Text style={styles.About}>Tentang Saya</Text>
         <TouchableOpacity>
            <Icon style={styles.Item} name='account-circle' size={200} color= '#EFEFEF'/>
          </TouchableOpacity>
          <Text style={styles.name}>Niken Pratiwi</Text>
          <Text style={styles.job}>React Native Developer</Text>
          <View style={styles.Portofolio}>
            <Text style={styles.TitleItem}>Portofolio</Text>
            <View style={{
               borderBottomColor: '#003366',
               borderBottomWidth: 1,
               marginVertical: 5
            }}/>
            <View style={styles.sosialmedia}>
              <View style={styles.sosialmediaItem}>
                <Icon style={styles.ItemSosialMedia} name ='gitlab' size= {50} color='#3EC6FF'/>
                <Text style={styles.sosialmediaTitle}>@niken.pratiwi218</Text>
              </View>
              <View style={styles.sosialmediaItem}>
                <Icon style={styles.ItemSosialMedia} name ='github' size= {50} color='#3EC6FF'/>
                <Text style={styles.sosialmediaTitle}>@niken.pratiwi218</Text>
              </View>
            </View>
          </View>
          <View style={styles.ContactMe}>
            <Text style={styles.TitleItem}>Hubungi Saya</Text>
            <View style={{
               borderBottomColor: '#003366',
               borderBottomWidth: 1,
               marginVertical: 5
            }}/>
            <View style={styles.ContactMeItem}>
              <Icon style={styles.ItemContanctMe} name='facebook' size= {50} color='#3EC5FF'/>
              <Text style={styles.contactMeTitle}>nikenpratiwicdp</Text>
            </View>
            <View style={styles.ContactMeItem}>
              <Icon style={styles.ItemContanctMe} name='instagram' size= {50} color='#3EC5FF'/>
              <Text style={styles.contactMeTitle}>@nikenpcdp</Text>
            </View>
            <View style={styles.ContactMeItem}>
              <Icon style={styles.ItemContanctMe} name='twitter' size= {50} color='#3EC5FF'/>
              <Text style={styles.contactMeTitle}>@nikenpecedepe</Text>
            </View>
          </View>
      </View>
      </ScrollView>
    )
  };
};
const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    paddingBottom: 50
  },
  About:{
    fontSize: 36,
    color: '#003366',
    fontWeight: "bold",
    paddingTop: 40
  },
  name:{
    fontSize: 24,
    fontWeight: "bold",
    color: '#003366',
  },
  job: {
    fontSize: 16,
    fontWeight: "bold",
    color: '#3EC6FF',
    marginTop: 5
  },
  Portofolio: {
    width: 359,
    height: 140,
    marginTop: 15,
    padding: 5,
    backgroundColor: '#EFEFEF',
    borderRadius: 16
  },
  TitleItem: {
    fontSize: 18,
    color: '#003366',
    marginLeft: 5
  },
  sosialmedia:{
    margin: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  sosialmediaItem:{
    alignItems: 'center',
    justifyContent: 'center'
  },
  sosialmediaTitle: {
    fontSize: 16,
    color: '#003366',
    fontWeight: "bold",
  },
  ContactMe:{
    width: 359,
    height:251,
    backgroundColor: '#EFEFEF',
    borderRadius:16,
    padding: 5,
    marginTop:10
  },
  ContactMeItem:{
    alignItems: 'center',
    marginVertical: 8,
    flexDirection: 'row',
  },
  ItemContanctMe:{
    marginLeft: 80
  },
  contactMeTitle: {
    fontSize: 16,
    color: '#003366',
    fontWeight: "bold",
    marginLeft: 20
  }
});