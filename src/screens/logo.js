import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';


export default class Logo extends React.Component{
  render() {
    return (
      <View style={StyleSheet.container}>
        <View style={styles.logo}>
          <Image source={require('../assets/image/Logo.png')} style={{width:150, height:150}}/>
        </View>
      </View>
    )
  };
};

const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 116,
    marginTop: 50,

  }
});